# Laboratorijske vježbe 6

Zadatak 1. Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost znanstvenog kalkulatora, 
odnosno implementirati osnovne (+,-,*,/) i barem 5 naprednih (sin, cos, log, sqrt...) operacija.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_Analiza
{
    public partial class Kalkulator : Form
    {
        double x, y, rez;
        public Kalkulator()
        {
            InitializeComponent();
        }

        private void button_Zbroji_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x)) 
            { 
                MessageBox.Show("Krivi unos prvog broja", "Pogreška"); 
            } 
            if (!double.TryParse(tB2.Text, out y)) 
            {
                MessageBox.Show("Krivi unos drugog broja", "Pogreška"); 
            }
            rez = x + y; 
        }

        private void button_Oduzmi_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x)) 
            { 
                MessageBox.Show("Krivi unos prvog broja", "Pogreška"); 
            } 
            if (!double.TryParse(tB2.Text, out y)) 
            { 
                MessageBox.Show("Krivi unos drugog broja", "Pogreška"); 
            } 
            rez = x - y; 
        }

        private void button_Pomnozi_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x)) 
            { 
                MessageBox.Show("Krivi unos prvog broja", "Pogreška"); 
            } 
            if (!double.TryParse(tB2.Text, out y) || y == 0) 
            { 
                MessageBox.Show("Krivi unos drugog broja", "Pogreška"); 
            } 
            rez = x * y;
        }

        private void button_Podijeli_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos prvog broja", "Pogreška");
            }
            if (!double.TryParse(tB2.Text, out y))
            {
                MessageBox.Show("Krivi unos drugog broja", "Pogreška");
            }
            rez = x / y;
        }

        private void button_sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos broja", "Pogreška");
            }
            rez = Math.Sin(x);
        }

        private void button_cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos broja", "Pogreška");
            }
            rez = Math.Cos(x);
        }

        private void button_log_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos broja", "Pogreška");
            }
            rez = Math.Log(x);
        }

        private void button_sqrt_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos broja", "Pogreška");
            }
            rez = Math.Sqrt(x);
        }

        private void button_pow_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB1.Text, out x))
            {
                MessageBox.Show("Krivi unos broja", "Pogreška");
            }
            rez = Math.Pow(x,y);
        }

        private void button_Izracunaj_Click(object sender, EventArgs e)
        {
            tBR.Text = rez.ToString();
            tBR.Show();
            tB1.Clear();
            tB2.Clear();
        }       
    }
}

Zadatak 2. Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu 
funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala, dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_Analiza
{
    public partial class Vjesalo : Form
    {
        Random rand = new Random();
        List<string> list = new List<string>();
        int brojPokusaja;
        string word, label;
        public void Reset()
        {
            word = list[rand.Next(0, list.Count - 1)];
            label = new string('*', word.Length);
            lbl_Rijec.Text = label;
            brojPokusaja = 9;
            lbl_Rez.Text = brojPokusaja.ToString();
        }
        public Vjesalo()
        {
            InitializeComponent();
        }
        private void btn_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void Vjesalo_Load(object sender, EventArgs e)
        {
            string line;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@"C:\Korisnici\korisnik\Dokumenti\Vješalo.txt"))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line);
                }
                Reset();
            }
        }
        private void btn_Unesi_Click(object sender, EventArgs e)
        {
            if (tB_unos.Text.Length == 1)
            {
                if (word.Contains(tB_unos.Text))
                {
                    string temp_rijec = word;
                    while (temp_rijec.Contains(tB_unos.Text))
                    {
                        int index = temp_rijec.IndexOf(tB_unos.Text);
                        StringBuilder builder = new StringBuilder(temp_rijec);
                        builder[index] = '*';
                        temp_rijec = builder.ToString();
                        StringBuilder builder1 = new StringBuilder(label);
                        builder1[index] = Convert.ToChar(tB_unos.Text);
                        label = builder1.ToString();
                    }
                    lbl_rijec.Text = label;
                    if (label == word)
                    {
                        MessageBox.Show("Pobjeda");
                        Reset();
                    }
                }
                else
                {
                    brojPokusaja--;
                    lbl_rez.Text = brojPokusaja.ToString();
                    if (brojPokusaja <= 0)
                    {
                        MessageBox.Show("Pokušaj ponovo!");
                        Reset();
                    }
                }
            }
            else if (tB_unos.Text.Length > 1)
            {
                if (word == tB_unos.Text)
                {
                    MessageBox.Show("Pobjeda");
                    Reset();
                }
                else
                {
                    brojPokusaja--;
                    lbl_rez.Text = brojPokusaja.ToString();
                    if (brojPokusaja <= 0)
                    {
                        MessageBox.Show("Pokušaj ponovo!");
                        Reset();
                    }
                }
            }
        }
    }
}